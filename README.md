# pet_dogbreeds

The task was to create a classification pipeline to classify 1 of 10 dog breeds on an image.

I used transfer learning method for this task and took ResNet50 with pretreined imagenet weights for training.

At the end of the training the accuracy metric was .93+ and f1 score was ~.93.

To get higher metrics I could have taken a bigger model and harder augmentations. 

The logs and final model can be obtained with the [LINK](https://drive.google.com/drive/folders/1bofoyMO9YxRFlowyfmocoI6bTH4waYoO?usp=sharing)
